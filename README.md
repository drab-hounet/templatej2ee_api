# TemplateJ2EE_API

*Développer sur l'ide NetBeans*

Template pour utiliser J2EE comme fournisseur de data 

Toutes les informations sont envoyées en format JSON

Exemple de route:

*http://localhost:8080/ProjectBackAPI/api/heroes*


```
[
  {
    "name": "hero1",
    "type": "guerrier"
  },
  {
    "name": "hero2",
    "type": "sorcier"
  },
  {
    "name": "hero3",
    "type": "guerrier"
  }
]
```


