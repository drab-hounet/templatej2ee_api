package com.game.api.heroes;

import com.game.activity.heroes.ActivityGetHeroes;
import static com.game.utils.APIUtils.displayJson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ApiHeroes", urlPatterns = {"/ApiHeroes"})
public class ApiHeroes extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ActivityGetHeroes activityGetHeroes = new ActivityGetHeroes();
        displayJson(response, activityGetHeroes.getHeroes());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
