package com.game.activity.heroes;

import com.game.model.Hero;
import java.util.ArrayList;

public class ActivityGetHeroes {

    public ArrayList<Hero> getHeroes() {
        
        // Exemple de code 
        // Partie réservée pour l'implémentation du code par l'étudiant
        
        ArrayList<Hero> heroes = new ArrayList<>();
        Hero hero1 = new Hero("hero1", "guerrier");
        Hero hero2 = new Hero("hero2", "sorcier");
        Hero hero3 = new Hero("hero3", "guerrier");
        heroes.add(hero1);
        heroes.add(hero2);
        heroes.add(hero3);
        return heroes;
    }
}
