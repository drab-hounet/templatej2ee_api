package com.game.model;

public class StateApi {

    private String statusAPI;
    private String token;
    private String log;

    public String getStatus() {
        return statusAPI;
    }

    public void setStatusOutSession() {
        this.statusAPI = "session out";
    }

    public void setStatusError() {
        this.statusAPI = "Error";
    }

    public void setStatusTokenError() {
        this.statusAPI = "token broken";
    }

    public void setStatusSuccess() {
        this.statusAPI = "success";
    }

    public void setStatusLogout() {
        this.statusAPI = "logout";
    }

    public void setStatusBadExtension() {
        this.statusAPI = "extension file not accepted";
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
