package com.game.utils;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;

public class APIUtils {

    public static void displayJson(HttpServletResponse response, ArrayList arrayList) throws IOException {
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        String json = gson.toJson(arrayList);
        out.print(json);
    }
}
